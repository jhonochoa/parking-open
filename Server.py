from Utilities import Constans
from Utilities import Services
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import StreamListener
from tweepy import Stream
import json



class ClientStdOutListener(StreamListener):  

	def on_data(self, data):
		
		print("Tamaño de StackTweets de tweets: " +str(len(Constans.StackTweets))) 
		#Añade a la lista los tweets
		if len(Constans.StackTweets) <Constans.TweetPopulation:
			Constans.StackTweets.append(data)
			print(len(Constans.StackTweets))

		elif len(Constans.StackTweets)==Constans.TweetPopulation:
			
			#Se envia mensaje al usuario que ya no se resiven mas tweets y se procesa	
			Services.sendToListReserver(Constans.StackTweets,authentication)
			Services.sendTweetOfNotificationFullTweets(data,authentication)

		return True

	def on_error(self,status):
		print(status)
		
	def on_status(self,status):
		print(status.text)



listener = ClientStdOutListener()
authentication = OAuthHandler(Constans.CONSUMER_KEY, Constans.CONSUMER_SECRET)
authentication.set_access_token(Constans.ACCES_TOKEN,Constans.ACCES_TOKEN_SECRET)

stream = Stream(authentication,listener)	
stream.filter(track=[Constans.Filter])