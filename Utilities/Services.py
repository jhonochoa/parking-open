import json
from Utilities import Constans
from Utilities import UtilStack
import tweepy
import time

#Stack para notificar
StackReserverNotification = []


def sendToListReserver(stackTweets,authentication):
	print("En metodo extractData")
	count =0
	if len(Constans.StackReserver)==Constans.TweetPopulation:
		pass
	else:
		while count<Constans.TweetPopulation:
			for contentTweets in stackTweets:
				#decode adiciona a la lista de reservas
				Constans.StackReserver.append(json.loads(contentTweets)["text"]+" "+str(json.loads(contentTweets)["user"]["screen_name"]))
				count = count+1

		distributionParking(Constans.StackReserver,authentication)

def sendTweetOfNotificationFullTweets(data,authentication):
	#Se crea el objeto para responder y la respuesta
	api = tweepy.API(authentication)
	statusTweetAnswer = api.update_status(str("@")+(json.loads(data)["user"]["screen_name"])+str(Constans.TweetFull)+str(time.strftime("%c")))
	time.sleep(2)
	print("Sorry ya se copo los tweets")

def sendTweetOfNotificationRserver(StackReserverNotification,authentication):
	print("Notificar a la poblacion")
	
	api = tweepy.API(authentication)
	#Se recorre la pila para notificar y se notifica a cada usuario
	for contenStackReserverNotification in StackReserverNotification:
		
		#Se valida si el usuario no cuenta con espacio y se le envia un menjase especial
		if contenStackReserverNotification[1]=="Z":
			statusTweetAnswer = api.update_status(str("@")+contenStackReserverNotification[0].split()[3]+str(Constans.TweetNorParking)+str(time.strftime("%c")))
			time.sleep(2)
		else:
			#Se le notifica al usuario cual es su zona de parqueo
			statusTweetAnswer = api.update_status(str("@")+contenStackReserverNotification[0].split()[3]+str(Constans.TweetParkingPartOne)+str(time.strftime("%c"))+str(Constans.TweetParkingPartTwo)+str(contenStackReserverNotification[1]))
			time.sleep(2)

def distributionParking(StackReserver,authentication):
	print("distributionParking")
	#Se ordenan ls lista de reservas y parqueo para determinar prioridad y la zona a parquear
	StackReserver.sort(reverse=True,key=UtilStack.takeKeyOrder)
	Constans.StackZoneParking.sort(reverse=True, key=UtilStack.takeKeyOrderParking)
	
	for contentReserver in StackReserver:
		#Se valida la diferencia entre el espacio para parquear y el tamaño del vehiculo
		print(Constans.StackZoneParking[0][1])
		if(float(Constans.StackZoneParking[0][1])-float(contentReserver.split()[1].strip("S")))<0.0:
			#Si la diferencia es menor que 0.0 se añade una letra diferente de las zonas de parqueo
			StackReserverNotification.append((contentReserver,"Z"))
			print("Da menor que cero")

		else:
			#Se actualiza el valor de la zona de parqueo
			Constans.StackZoneParking[0] = (Constans.StackZoneParking[0][0],float(Constans.StackZoneParking[0][1])-float(contentReserver.split()[1].strip("S")))			
			#Se añade al Stack para notificar
			StackReserverNotification.append((contentReserver,Constans.StackZoneParking[0][0]))
			#Se ordena nuevamente Stack de parqueo
			Constans.StackZoneParking.sort(reverse=True, key=UtilStack.takeKeyOrderParking)
			print(Constans.StackZoneParking)
	print(StackReserverNotification)
	#Se envia a notificar a la población
	sendTweetOfNotificationRserver(StackReserverNotification,authentication)


